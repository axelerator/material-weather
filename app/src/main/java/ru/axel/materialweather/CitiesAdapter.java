package ru.axel.materialweather;

import android.support.annotation.StringDef;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by axel on 15.06.2016.
 */
public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {
    private String[] mDataset;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View CityWeatherView;
        public TextView CityName;
        public TextView CityTemperature;

        public ViewHolder(View view) {
            super(view);
            CityWeatherView = view;
            CityName = (TextView) view.findViewById(R.id.tv_city_name);
            CityTemperature = (TextView) view.findViewById(R.id.tv_city_temperature);
        }
    }

    public CitiesAdapter(String[] myDataset) {
        mDataset = myDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.citieslist_weather_card, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // TODO: 16.06.2016 доработать
        holder.CityName.setText("Kazan");
        holder.CityTemperature.setText("+256");
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }
}
